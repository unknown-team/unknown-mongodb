FROM unknown-alpine:3.6
LABEL maintainer Alexander 'Polynomdivision'

RUN apk add --no-cache mongodb

# Create the user and group
RUN deluser mongodb \
    && addgroup -g 1002 mongodb \
    && adduser -D -H -u 1002 -G mongodb mongodb

# Create config directories
RUN mkdir -p /data/db /data/configdb \
    && chown -R mongodb:mongodb /data/db /data/configdb
VOLUME /data/db /data/configdb

COPY ./container/start-mongodb /usr/local/bin/start-mongo
RUN chmod 500 /usr/local/bin/start-mongo

# Document the exposed port
EXPOSE 27017
ENTRYPOINT ["/usr/local/bin/start-mongo"]
